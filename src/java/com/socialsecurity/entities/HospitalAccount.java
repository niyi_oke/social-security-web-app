/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.socialsecurity.entities;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;

/**
 *
 * @author BigBen
 */
@NamedQuery(name = "findHospitalAccoutByEmailAndPassword", query = "SELECT h FROM HospitalAccount h WHERE h.hospitalEmail=:hospitalEmail AND h.password=:password")
@Entity
public class HospitalAccount implements Serializable {
    
    private static final long serialVersionUID = -4419277033298795448L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long accountId;
    
    private String hospitalName;
    
    private String password;
    
    private String hospitalEmail;
    
    private String hospitalLicenseNumber;
    
    private String hospitalAddress;
    
    public HospitalAccount() {
    }

    public HospitalAccount(String hospitalName, String password, String hospitalEmail, String hospitalLicenseNumber, String hospitalAddress) {
        this.hospitalName = hospitalName;
        this.password = password;
        this.hospitalEmail = hospitalEmail;
        this.hospitalLicenseNumber = hospitalLicenseNumber;
        this.hospitalAddress = hospitalAddress;
    }

    public Long getAccountId() {
        return accountId;
    }

    public String getHospitalName() {
        return hospitalName;
    }

    public void setHospitalName(String hospitalName) {
        this.hospitalName = hospitalName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getHospitalEmail() {
        return hospitalEmail;
    }

    public void setHospitalEmail(String hospitalEmail) {
        this.hospitalEmail = hospitalEmail;
    }

    public String getHospitalLicenseNumber() {
        return hospitalLicenseNumber;
    }

    public void setHospitalLicenseNumber(String hospitalLicenseNumber) {
        this.hospitalLicenseNumber = hospitalLicenseNumber;
    }

    public String getHospitalAddress() {
        return hospitalAddress;
    }

    public void setHospitalAddress(String hospitalAddress) {
        this.hospitalAddress = hospitalAddress;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + Objects.hashCode(this.accountId);
        hash = 97 * hash + Objects.hashCode(this.hospitalName);
        hash = 97 * hash + Objects.hashCode(this.password);
        hash = 97 * hash + Objects.hashCode(this.hospitalEmail);
        hash = 97 * hash + Objects.hashCode(this.hospitalLicenseNumber);
        hash = 97 * hash + Objects.hashCode(this.hospitalAddress);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final HospitalAccount other = (HospitalAccount) obj;
        if (!Objects.equals(this.hospitalName, other.hospitalName)) {
            return false;
        }
        if (!Objects.equals(this.password, other.password)) {
            return false;
        }
        if (!Objects.equals(this.hospitalEmail, other.hospitalEmail)) {
            return false;
        }
        if (!Objects.equals(this.hospitalLicenseNumber, other.hospitalLicenseNumber)) {
            return false;
        }
        if (!Objects.equals(this.hospitalAddress, other.hospitalAddress)) {
            return false;
        }
        if (!Objects.equals(this.accountId, other.accountId)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "HospitalAccount{" + "accountId=" + accountId + ", hosptialName=" + hospitalName + ", password=" + password + ", hospitalEmail=" + hospitalEmail + ", hospitalLicenseNumber=" + hospitalLicenseNumber + ", hospitalAddress=" + hospitalAddress + '}';
    }

    
}
