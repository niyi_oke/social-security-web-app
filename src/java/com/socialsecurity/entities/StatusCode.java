/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.socialsecurity.entities;


/**
 *
 * @author BigBen
 */
public enum StatusCode {
    SUCCESSFUL(200),NOT_SUCCESSFUL(300),EMPTY_OR_NULL_PARAMETER(201);
    
    private final int code;

    private StatusCode(int code) {
        this.code = code;
    }

    public int getCode() {
        return code;
    }

    
}
