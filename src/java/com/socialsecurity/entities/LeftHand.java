/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.socialsecurity.entities;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author BigBen
 */
@Entity(name = "Left_Hand")
@XmlRootElement
public class LeftHand implements Serializable {
    
    private static final long serialVersionUID = 5491698870594885763L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long leftHandId;
    
    private String thumbImagePath;
    
    private String indexImagePath;
    
    private String RingFingerImagePath;
    
    private String littleFingerImagePath;
    @OneToOne(mappedBy = "leftHand")
    private Child child;

    public LeftHand() {
    }

    public LeftHand(String thumbImagePath, String indexImagePath, String RingFingerImagePath, String littleFingerImagePath) {
        this.thumbImagePath = thumbImagePath;
        this.indexImagePath = indexImagePath;
        this.RingFingerImagePath = RingFingerImagePath;
        this.littleFingerImagePath = littleFingerImagePath;
    }

    public Long getLeftHandId() {
        return leftHandId;
    }

    public void setLeftHandId(Long leftHandId) {
        this.leftHandId = leftHandId;
    }

    public String getThumbImagePath() {
        return thumbImagePath;
    }

    public void setThumbImagePath(String thumbImagePath) {
        this.thumbImagePath = thumbImagePath;
    }

    public String getIndexImagePath() {
        return indexImagePath;
    }

    public void setIndexImagePath(String indexImagePath) {
        this.indexImagePath = indexImagePath;
    }

    public String getRingFingerImagePath() {
        return RingFingerImagePath;
    }

    public void setRingFingerImagePath(String RingFingerImagePath) {
        this.RingFingerImagePath = RingFingerImagePath;
    }

    public String getLittleFingerImagePath() {
        return littleFingerImagePath;
    }

    public void setLittleFingerImagePath(String littleFingerImagePath) {
        this.littleFingerImagePath = littleFingerImagePath;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 73 * hash + Objects.hashCode(this.leftHandId);
        hash = 73 * hash + Objects.hashCode(this.thumbImagePath);
        hash = 73 * hash + Objects.hashCode(this.indexImagePath);
        hash = 73 * hash + Objects.hashCode(this.RingFingerImagePath);
        hash = 73 * hash + Objects.hashCode(this.littleFingerImagePath);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final LeftHand other = (LeftHand) obj;
        if (!Objects.equals(this.thumbImagePath, other.thumbImagePath)) {
            return false;
        }
        if (!Objects.equals(this.indexImagePath, other.indexImagePath)) {
            return false;
        }
        if (!Objects.equals(this.RingFingerImagePath, other.RingFingerImagePath)) {
            return false;
        }
        if (!Objects.equals(this.littleFingerImagePath, other.littleFingerImagePath)) {
            return false;
        }
        if (!Objects.equals(this.leftHandId, other.leftHandId)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "LeftHand{" + "leftHandId=" + leftHandId + ", thumbImagePath=" + thumbImagePath + ", indexImagePath=" + indexImagePath + ", RingFingerImagePath=" + RingFingerImagePath + ", littleFingerImagePath=" + littleFingerImagePath + '}';
    }
    
    
}
