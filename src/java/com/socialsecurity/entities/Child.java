/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.socialsecurity.entities;

import java.io.File;
import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author BigBen
 */
@NamedQuery(name = "findChildbyChildName&SocialNumber",query = "SELECT c FROM Child c WHERE c.childName=:childName AND c.socialNumber=:socialNumber")
@Entity
@XmlRootElement
public class Child implements Serializable {

    private static final long serialVersionUID = 4629178304833038140L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long childId;

    private String childName;

    private String fatherName;

    private String motherName;

    private String motherEmail;

    private String phoneNo;

    private ChildSex sex;

    private String datetimeOfBirth;

    private String socialNumber;

    private String certifierName;

    private String certifierMedicalTitle;

    private String datetimeOfRegistration;

    private String birthPlaceAddress;
    
    private File childImage;

    @OneToOne
    private RightHand rightHand;

    @OneToOne
    private LeftHand leftHand;

    public Child() {
    }

    public Child(String childName, String fatherName, String motherName, String motherEmail, String phoneNo, ChildSex sex, String datetimeOfBirth, String socialNumber, String certifierName, String certifierMedicalTitle, String datetimeOfRegistration, String birthPlaceAddress, File childImage, RightHand rightHand, LeftHand leftHand) {
        this.childName = childName;
        this.fatherName = fatherName;
        this.motherName = motherName;
        this.motherEmail = motherEmail;
        this.phoneNo = phoneNo;
        this.sex = sex;
        this.datetimeOfBirth = datetimeOfBirth;
        this.socialNumber = socialNumber;
        this.certifierName = certifierName;
        this.certifierMedicalTitle = certifierMedicalTitle;
        this.datetimeOfRegistration = datetimeOfRegistration;
        this.birthPlaceAddress = birthPlaceAddress;
        this.childImage = childImage;
        this.rightHand = rightHand;
        this.leftHand = leftHand;
    }

    
    public Long getChildId() {
        return childId;
    }

    public String getChildName() {
        return childName;
    }

    public void setChildName(String childName) {
        this.childName = childName;
    }

    public String getFatherName() {
        return fatherName;
    }

    public void setFatherName(String fatherName) {
        this.fatherName = fatherName;
    }

    public String getMotherName() {
        return motherName;
    }

    public void setMotherName(String motherName) {
        this.motherName = motherName;
    }

    public String getMotherEmail() {
        return motherEmail;
    }

    public void setMotherEmail(String motherEmail) {
        this.motherEmail = motherEmail;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public ChildSex getSex() {
        return sex;
    }

    public void setSex(ChildSex sex) {
        this.sex = sex;
    }

    public String getDatetimeOfBirth() {
        return datetimeOfBirth;
    }

    public void setDatetimeOfBirth(String datetimeOfBirth) {
        this.datetimeOfBirth = datetimeOfBirth;
    }

    public String getSocialNumber() {
        return socialNumber;
    }

    public void setSocialNumber(String socialNumber) {
        this.socialNumber = socialNumber;
    }

    public String getCertifierName() {
        return certifierName;
    }

    public void setCertifierName(String certifierName) {
        this.certifierName = certifierName;
    }

    public String getCertifierMedicalTitle() {
        return certifierMedicalTitle;
    }

    public void setCertifierMedicalTitle(String certifierMedicalTitle) {
        this.certifierMedicalTitle = certifierMedicalTitle;
    }

    public String getDatetimeOfRegistration() {
        return datetimeOfRegistration;
    }

    public void setDatetimeOfRegistration(String datetimeOfRegistration) {
        this.datetimeOfRegistration = datetimeOfRegistration;
    }

    public String getBirthPlaceAddress() {
        return birthPlaceAddress;
    }

    public void setBirthPlaceAddress(String birthPlaceAddress) {
        this.birthPlaceAddress = birthPlaceAddress;
    }

    public File getChildImage() {
        return childImage;
    }

    public void setChildImage(File childImage) {
        this.childImage = childImage;
    }

    public RightHand getRightHand() {
        return rightHand;
    }

    public void setRightHand(RightHand rightHand) {
        this.rightHand = rightHand;
    }

    public LeftHand getLeftHand() {
        return leftHand;
    }

    public void setLeftHand(LeftHand leftHand) {
        this.leftHand = leftHand;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 79 * hash + Objects.hashCode(this.childId);
        hash = 79 * hash + Objects.hashCode(this.childName);
        hash = 79 * hash + Objects.hashCode(this.fatherName);
        hash = 79 * hash + Objects.hashCode(this.motherName);
        hash = 79 * hash + Objects.hashCode(this.motherEmail);
        hash = 79 * hash + Objects.hashCode(this.phoneNo);
        hash = 79 * hash + Objects.hashCode(this.sex);
        hash = 79 * hash + Objects.hashCode(this.datetimeOfBirth);
        hash = 79 * hash + Objects.hashCode(this.socialNumber);
        hash = 79 * hash + Objects.hashCode(this.certifierName);
        hash = 79 * hash + Objects.hashCode(this.certifierMedicalTitle);
        hash = 79 * hash + Objects.hashCode(this.datetimeOfRegistration);
        hash = 79 * hash + Objects.hashCode(this.birthPlaceAddress);
        hash = 79 * hash + Objects.hashCode(this.childImage);
        hash = 79 * hash + Objects.hashCode(this.rightHand);
        hash = 79 * hash + Objects.hashCode(this.leftHand);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Child other = (Child) obj;
        if (!Objects.equals(this.childName, other.childName)) {
            return false;
        }
        if (!Objects.equals(this.fatherName, other.fatherName)) {
            return false;
        }
        if (!Objects.equals(this.motherName, other.motherName)) {
            return false;
        }
        if (!Objects.equals(this.motherEmail, other.motherEmail)) {
            return false;
        }
        if (!Objects.equals(this.phoneNo, other.phoneNo)) {
            return false;
        }
        if (!Objects.equals(this.datetimeOfBirth, other.datetimeOfBirth)) {
            return false;
        }
        if (!Objects.equals(this.socialNumber, other.socialNumber)) {
            return false;
        }
        if (!Objects.equals(this.certifierName, other.certifierName)) {
            return false;
        }
        if (!Objects.equals(this.certifierMedicalTitle, other.certifierMedicalTitle)) {
            return false;
        }
        if (!Objects.equals(this.datetimeOfRegistration, other.datetimeOfRegistration)) {
            return false;
        }
        if (!Objects.equals(this.birthPlaceAddress, other.birthPlaceAddress)) {
            return false;
        }
        if (!Objects.equals(this.childId, other.childId)) {
            return false;
        }
        if (this.sex != other.sex) {
            return false;
        }
        if (!Objects.equals(this.childImage, other.childImage)) {
            return false;
        }
        if (!Objects.equals(this.rightHand, other.rightHand)) {
            return false;
        }
        return Objects.equals(this.leftHand, other.leftHand);
    }

    @Override
    public String toString() {
        return "Child{" + "childId=" + childId + ", childName=" + childName + ", fatherName=" + fatherName + ", motherName=" + motherName + ", motherEmail=" + motherEmail + ", phoneNo=" + phoneNo + ", sex=" + sex + ", datetimeOfBirth=" + datetimeOfBirth + ", socialNumber=" + socialNumber + ", certifierName=" + certifierName + ", certifierMedicalTitle=" + certifierMedicalTitle + ", datetimeOfRegistration=" + datetimeOfRegistration + ", birthPlaceAddress=" + birthPlaceAddress + ", childImage=" + childImage + ", rightHand=" + rightHand + ", leftHand=" + leftHand + '}';
    }

}
