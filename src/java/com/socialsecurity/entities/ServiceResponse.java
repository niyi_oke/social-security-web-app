/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.socialsecurity.entities;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author BigBen
 * @param <Entity>
 */
@XmlRootElement(name = "service_response")
public class ServiceResponse<Entity> implements Serializable{
    
    private static final long serialVersionUID = 654130837170505642L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long baseResponseId;
    
    private StatusCode status;
            
    private Entity entity;
    
    private String message;

    public ServiceResponse() {
    }

    public ServiceResponse(StatusCode status, String message, Entity entity) {
        this.status = status;
        this.entity = entity;
        this.message = message;
    }

    public long getBaseResponseId() {
        return baseResponseId;
    }

    public void setBaseResponseId(long baseResponseId) {
        this.baseResponseId = baseResponseId;
    }

    public StatusCode getStatus() {
        return status;
    }

    public void setStatus(StatusCode status) {
        this.status = status;
    }

    public Entity getEntity() {
        return entity;
    }

    public void setEntity(Entity entity) {
        this.entity = entity;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 47 * hash + (int) (this.baseResponseId ^ (this.baseResponseId >>> 32));
        hash = 47 * hash + Objects.hashCode(this.status);
        hash = 47 * hash + Objects.hashCode(this.entity);
        hash = 47 * hash + Objects.hashCode(this.message);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ServiceResponse<?> other = (ServiceResponse<?>) obj;
        if (this.baseResponseId != other.baseResponseId) {
            return false;
        }
        if (!Objects.equals(this.message, other.message)) {
            return false;
        }
        if (this.status != other.status) {
            return false;
        }
        if (!Objects.equals(this.entity, other.entity)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ServiceResponse{" + "baseResponseId=" + baseResponseId + ", status=" + status + ", entity=" + entity + ", message=" + message + '}';
    }
    
}
