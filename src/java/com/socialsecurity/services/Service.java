/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.socialsecurity.services;

import com.github.sarxos.webcam.Webcam;
import com.github.sarxos.webcam.WebcamDevice;
import com.github.sarxos.webcam.WebcamResolution;
import com.socialsecurity.entities.Child;
import com.socialsecurity.entities.ChildSex;
import com.socialsecurity.entities.HospitalAccount;
import com.socialsecurity.entities.ServiceResponse;
import com.socialsecurity.entities.StatusCode;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.imageio.ImageIO;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author BigBen
 */
@Stateless
public class Service {

    @PersistenceContext
    private EntityManager em;
    
    private Webcam webCam;

    public ServiceResponse createHospitalAccount(String hosptialName, String password, String hospitalEmail, String hospitalLicenseNumber, String hospitalAddress) {
        List<HospitalAccount> emails = em.createNamedQuery("findHospitalAccoutByEmail", HospitalAccount.class).setParameter("hospitalEmail", hospitalEmail).getResultList();
        if (!emails.isEmpty()) {
            ServiceResponse sr = new ServiceResponse(StatusCode.NOT_SUCCESSFUL, "This hospital account already exists", null);
            return sr;
        }
        HospitalAccount account = new HospitalAccount();
        account.setHospitalName(hosptialName);
        account.setHospitalEmail(hospitalEmail);
        account.setPassword(password);
        account.setHospitalAddress(hospitalAddress);
        account.setHospitalLicenseNumber(hospitalLicenseNumber);

        em.persist(account);
        ServiceResponse sr = new ServiceResponse(StatusCode.SUCCESSFUL, "Your Hospital account has been created", account);
        return sr;
    }

    public ServiceResponse login(String hospitalEmail, String password) {

        List<HospitalAccount> accounts = em.createNamedQuery("findHospitalAccoutByEmailAndPassword", HospitalAccount.class).setParameter("hospitalEmail", hospitalEmail).setParameter("password", password).getResultList();
        if (accounts.isEmpty()) {
            ServiceResponse sr = new ServiceResponse(StatusCode.NOT_SUCCESSFUL, "No such Hospital Account exists", null);
            return sr;
        }
        HospitalAccount account = accounts.get(0);
        ServiceResponse sr = new ServiceResponse(StatusCode.SUCCESSFUL, "Logged in", account);
        return sr;
    }

    public ServiceResponse registerChild(String childName, String fatherName, String motherName, String motherEmail, String phoneNo, ChildSex sex, String dateOfBirth, String socialNumber, String certifierName, String certifierMedicalTitle, String dateOfRegistration, String birthPlaceAddress,File childImage) {
        List<Child> children = em.createNamedQuery("findChildbyChildName&SocialNumber", Child.class)
                .setParameter("childName", childName).setParameter("socialNumber", socialNumber).getResultList();
        if (!children.isEmpty()) {
            ServiceResponse sr = new ServiceResponse(StatusCode.NOT_SUCCESSFUL, "The child with name "
                    + children.get(0).getChildName() + " and social number " + children.get(0).getSocialNumber()
                    + " has been registered already", children.get(0).getChildName());
            return sr;
        }
        Child child = new Child();
        child.setChildName(childName);
        child.setMotherName(motherName);
        child.setFatherName(fatherName);
        child.setMotherEmail(motherEmail);
        child.setSex(sex);
        child.setSocialNumber(socialNumber);
        child.setDatetimeOfBirth(dateOfBirth);
        child.setPhoneNo(phoneNo);
        child.setBirthPlaceAddress(birthPlaceAddress);
        child.setCertifierName(certifierName);
        child.setCertifierMedicalTitle(certifierMedicalTitle);
        child.setDatetimeOfRegistration(dateOfRegistration);
        child.setLeftHand(null);
        child.setRightHand(null);
        child.setChildImage(childImage);
        em.persist(child);
        ServiceResponse sr = new ServiceResponse(StatusCode.SUCCESSFUL, child.getChildName()+" has been successfully registered", child);
        return sr;
    }
    
    public File captureImage(){
        webCam = Webcam.getDefault();
        webCam.setViewSize(WebcamResolution.VGA.getSize());
        webCam.open();
        BufferedImage image = webCam.getImage();
        File file = new File("C:\\Users\\BigBen\\Documents\\NetBeansProjects\\SocialSecurity\\src\\java\\com\\socialsecurity\\resource");
        try {
            ImageIO.write(image, "PNG", file);
        } catch (IOException ex) {
            Logger.getLogger(Service.class.getName()).log(Level.SEVERE, null, ex);
        }
        webCam.close();
        return file;
    }

    // Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Business Method")
}
