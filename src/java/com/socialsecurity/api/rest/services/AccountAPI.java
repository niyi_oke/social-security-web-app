
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.socialsecurity.api.rest.services;

import com.socialsecurity.entities.ServiceResponse;
import com.socialsecurity.entities.StatusCode;
import com.socialsecurity.services.Service;
import java.util.Objects;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author BigBen
 */
@Path("account")
@Stateless
public class AccountAPI {

    @EJB
    private Service service;

    @Path("create")
    @POST
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response createAccount(@QueryParam("hospitalName") String hospitalName, @QueryParam("password") String password, @QueryParam("hospitalEmail") String hospitalEmail, @QueryParam("hospitalLicenseNumber") String hospitalLicenseNumber, @QueryParam("hospitalAddress") String hospitalAddress) {
        if (Objects.isNull(hospitalName) || hospitalName.isEmpty()
                || Objects.isNull(hospitalEmail) || hospitalEmail.isEmpty()
                || Objects.isNull(password) || password.isEmpty()
                || Objects.isNull(hospitalLicenseNumber) || hospitalLicenseNumber.isEmpty()
                || Objects.isNull(hospitalAddress) || hospitalAddress.isEmpty()) {
            ServiceResponse sr = new ServiceResponse(StatusCode.EMPTY_OR_NULL_PARAMETER, "Invalid Parameter(s)", null);
            return Response.ok(sr).build();
        }
        return Response.ok(service.createHospitalAccount(hospitalName, password, hospitalEmail, hospitalLicenseNumber, hospitalAddress)).build();

    }

    @Path("login")
    @GET
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response login(@QueryParam("hospitalEmail") String hospitalEmail, @QueryParam("password") String password) {
        if (Objects.isNull(hospitalEmail) || Objects.isNull(password)) {
            ServiceResponse sr = new ServiceResponse(StatusCode.EMPTY_OR_NULL_PARAMETER, "Email or Password is Incorrect", null);
            return Response.accepted(sr).build();
        }
        return Response.ok(service.login(hospitalEmail, password)).build();
    }
    
    

}
