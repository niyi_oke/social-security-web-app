/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.socialsecurity.api.rest.services;

import com.socialsecurity.entities.ChildSex;
import com.socialsecurity.entities.ServiceResponse;
import com.socialsecurity.entities.StatusCode;
import com.socialsecurity.services.Service;
import java.io.File;
import java.util.Objects;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author BigBen
 */
@Path("child")
@Stateless
public class ChildAPI {
    
    @EJB
    private Service service;
    
    @Path("register")
    @POST
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response registerChild(@QueryParam("childName")String childName,@QueryParam("fatherName") String fatherName,@QueryParam("motherName") String motherName,@QueryParam("motherEmail") String motherEmail,
            @QueryParam("phoneNo")String phoneNo,@QueryParam("sex") ChildSex sex,@QueryParam("dateOfBirth") String dateOfBirth,@QueryParam("socialNumber") String socialNumber,@QueryParam("certifierName") String certifierName,
            @QueryParam("certifierMedicalTitle")String certifierMedicalTitle,@QueryParam("dateOfRegistration") String dateOfRegistration,@QueryParam("birthPlaceAddress") String birthPlaceAddress,@QueryParam("childImgae") File childImage){
        if(Objects.isNull(childName) || childName.isEmpty()){
            ServiceResponse sr = new ServiceResponse(StatusCode.EMPTY_OR_NULL_PARAMETER, "child Name is incorrect", null);
            return Response.ok(sr).build();
        }
        if(Objects.isNull(fatherName) || fatherName.isEmpty()){
            ServiceResponse sr = new ServiceResponse(StatusCode.EMPTY_OR_NULL_PARAMETER, "Father's name is incorrect", null);
            return Response.ok(sr).build();
        }
        if(Objects.isNull(motherName) || motherName.isEmpty()){
            ServiceResponse sr = new ServiceResponse(StatusCode.EMPTY_OR_NULL_PARAMETER, "Mother's name is incorrect", null);
            return Response.ok(sr).build();
        }
        if(Objects.isNull(motherEmail) || motherEmail.isEmpty()){
            ServiceResponse sr = new ServiceResponse(StatusCode.EMPTY_OR_NULL_PARAMETER, "Mother's email is incorrect", null);
            return Response.ok(sr).build();
        }
        if(Objects.isNull(phoneNo) || phoneNo.isEmpty()){
            ServiceResponse sr = new ServiceResponse(StatusCode.EMPTY_OR_NULL_PARAMETER, "phone number is incorrect", null);
            return Response.ok(sr).build();
        }
        if(Objects.isNull(sex)){
            ServiceResponse sr = new ServiceResponse(StatusCode.EMPTY_OR_NULL_PARAMETER, "sex is incorrect", null);
            return Response.ok(sr).build();
        }
        if(Objects.isNull(dateOfBirth) || dateOfBirth.isEmpty()){
            ServiceResponse sr = new ServiceResponse(StatusCode.EMPTY_OR_NULL_PARAMETER, "Date of birth is incorrect", null);
            return Response.ok(sr).build();
        }
        if(Objects.isNull(certifierName) || certifierName.isEmpty()){
            ServiceResponse sr = new ServiceResponse(StatusCode.EMPTY_OR_NULL_PARAMETER, "Certifier name is incorrect", null);
            return Response.ok(sr).build();
        }
        if(Objects.isNull(certifierMedicalTitle) || certifierMedicalTitle.isEmpty()){
            ServiceResponse sr = new ServiceResponse(StatusCode.EMPTY_OR_NULL_PARAMETER, "Certifier Medical Title is incorrect", null);
            return Response.ok(sr).build();
        }
        if(Objects.isNull(birthPlaceAddress) || birthPlaceAddress.isEmpty()){
            ServiceResponse sr = new ServiceResponse(StatusCode.EMPTY_OR_NULL_PARAMETER, "Birth place address is incorrect", null);
            return Response.ok(sr).build();
        }
        if(Objects.isNull(dateOfRegistration) || dateOfRegistration.isEmpty()){
            ServiceResponse sr = new ServiceResponse(StatusCode.EMPTY_OR_NULL_PARAMETER, "Date of Registration is incorrect", null);
            return Response.ok(sr).build();
        }
        if(Objects.isNull(childImage)){
            ServiceResponse sr = new ServiceResponse(StatusCode.EMPTY_OR_NULL_PARAMETER, "No child image", null);
            return Response.ok(sr).build();
        }
        
        return Response.ok(service.registerChild(childName, fatherName, motherName, motherEmail, phoneNo, sex, dateOfBirth,
                socialNumber, certifierName, certifierMedicalTitle, dateOfRegistration, birthPlaceAddress,childImage)).build();
    }
    
    @Path("capture")
    @GET
    public Response captureImage(){
      return Response.ok(service.captureImage()).build();
    }
    
}
