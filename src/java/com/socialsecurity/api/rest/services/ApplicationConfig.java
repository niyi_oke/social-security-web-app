/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.socialsecurity.api.rest.services;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 *
 * @author BigBen
 */
@ApplicationPath("api/rest")
public class ApplicationConfig extends Application{
    
}
